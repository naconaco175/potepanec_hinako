require 'rails_helper'

RSpec.describe "ApplicationHelper", type: :helper do
  include ApplicationHelper

  describe "full_title(page_title)" do
    it 'when page_title is exist' do
      page_title = "title"
      expect(full_title(page_title)).to eq "title - BIGBAG Store"
    end

    it 'when page_title is blank' do
      page_title = ""
      expect(full_title(page_title)).to eq "BIGBAG Store"
    end

    it 'when page_title is nil' do
      page_title = nil
      expect(full_title(page_title)).to eq "BIGBAG Store"
    end
  end
end
