require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxons) { create_list(:taxon, 5, taxonomy: taxonomy) }
    let(:related_product) { create_list(:product, 5, taxons: taxons) }
    let(:product) { create(:product, taxons: taxons) }
    let(:related_products) { product.related_products }

    before do
      get :show, params: { id: product.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "assigns the requested product to @product" do
      expect(assigns(:product)).to eq product
    end

    it "assigns the requested related products to @related_products" do
      expect(assigns(:related_products)).to eq [related_product[0], related_product[1], related_product[2], related_product[3]]
    end

    it "render the :show template" do
      expect(response).to render_template :show
    end
  end
end
