require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:category) { create(:taxonomy, position: 1) }
    let!(:brand) { create(:taxonomy, position: 2) }
    let(:taxon)    { create(:taxon) }
    let(:products) { create_list(:product, 2, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status 200
    end

    it "assigns the requested category to @taxonomies " do
      expect(assigns(:taxonomies).first).to eq category
    end

    it "assigns the requested brand to @taxonomies" do
      expect(assigns(:taxonomies).second).to eq brand
    end

    it "assigns the requested taxon to @taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "assigns the requested products to @products" do
      expect(assigns(:products)).to eq products
    end

    it "render the :show template" do
      expect(response).to render_template :show
    end
  end
end
