require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "related_products" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxons) { create_list(:taxon, 4, taxonomy: taxonomy) }
    let(:related_products) { create_list(:product, 4, taxons: taxons) }
    let(:no_related_product) { create(:product) }
    let(:main_product) { create(:product, taxons: taxons) }
    let(:related_products_on_main_product) { main_product.related_products }

    context "related_products array have only related products" do
      it "have 4 related_product" do
        expect(related_products_on_main_product).to include related_products[0], related_products[1], related_products[2], related_products[3]
      end

      it "have not the no_related_product" do
        expect(related_products_on_main_product).not_to include no_related_product
      end

      it "have not the main_product" do
        expect(related_products_on_main_product).not_to include main_product
      end
    end

    it "have no same product in related_products array" do
      expect(related_products_on_main_product).to eq related_products_on_main_product.uniq
    end
  end
end
