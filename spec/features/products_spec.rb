require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxons) { create_list(:taxon, 5, taxonomy: taxonomy) }
  let(:products) { create_list(:product, 6, taxons: taxons) }
  let(:main_product) { products[0] }
  let(:related_products) { [products[1], products[2], products[3], products[4], products[5]] }
  let(:relatedproductBoxs) { page.all(".relatedproductBox") }

  before do
    visit potepan_product_path(main_product.id)
  end

  # 商品の詳細を表示する
  it "view products" do
    within '.product-info' do
      expect(page).to have_content main_product.name
      expect(page).to have_content main_product.display_price
      expect(page).to have_content main_product.description
    end
  end

  describe "has three links which back to top page" do
    it "HOME in header" do
      within '.nav' do
        click_link "HOME"
        expect(current_path).to eq potepan_path
      end
    end

    it "logo image" do
      click_on 'BIGBAG'
      expect(current_path).to eq potepan_path
    end

    it "HOME in breadcrumb" do
      within '.breadcrumb' do
        click_link "HOME"
        expect(current_path).to eq potepan_path
      end
    end
  end

  # 一覧ページへ戻るボタンで、その商品のカテゴリーページへ遷移する
  it 'back to category page' do
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxons[0].id)
    within first('.productBox') do
      expect(page).to have_content main_product.name
      expect(page).to have_content main_product.display_price
      expect(page).to have_selector('img[alt="products-img"]')
    end
  end

  describe "related products" do
    it "have 4 related products in order by taxon.id" do
      expect(relatedproductBoxs[0].find('h5').text).to eq related_products[0].name
      expect(relatedproductBoxs[1].find('h5').text).to eq related_products[1].name
      expect(relatedproductBoxs[2].find('h5').text).to eq related_products[2].name
      expect(relatedproductBoxs[3].find('h5').text).to eq related_products[3].name
    end

    it "have a name, price ,image and path to own product_path" do
      within first('.relatedproductBox') do
        expect(page).to have_content related_products[0].name
        expect(page).to have_content related_products[0].display_price
        expect(page).to have_selector('img[alt="products-img"]')
        expect(page).not_to have_content related_products[4].name
      end
      click_on related_products[0].name
      expect(current_path).to eq potepan_product_path(related_products[0].id)
    end
  end
end
