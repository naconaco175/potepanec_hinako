require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categorie") }
  let(:taxon) { create(:taxon, name: "Brand", parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it " move from category page to product page" do
    within '.side-nav' do
      click_link taxonomy.name
      visit current_path
      click_link taxon.name
    end
    expect(current_path).to eq potepan_category_path(taxon.id)

    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_selector('img[alt="products-img"]')
      click_link product.name
    end
    expect(current_path).to eq potepan_product_path(product.id)
  end

  describe "has two category names in header" do
    it "in bleft side header" do
      within '.page-title' do
        expect(page).to have_content taxon.name
      end
    end

    it "in breadcrumb" do
      within '.breadcrumb' do
        expect(page).to have_content taxon.name
      end
    end
  end

  describe "has three links which back to top page" do
    it "HOME in header" do
      within '.navbar-right' do
        click_link "HOME"
        expect(current_path).to eq potepan_path
      end
    end

    it "logo image" do
      click_on 'BIGBAG'
      expect(current_path).to eq potepan_path
    end

    it "HOME in breadcrumb" do
      within '.breadcrumb' do
        click_link "HOME"
        expect(current_path).to eq potepan_path
      end
    end
  end
end
